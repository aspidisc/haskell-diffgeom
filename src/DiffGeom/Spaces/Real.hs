{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}

{- |Custom 'Real' type that's just a newtype over 'Double'.

NOTE: this is old code and probably not actually necessary, as we could just add
instances of 'DiffReal' for any 'RealFloat'.
-}

module DiffGeom.Spaces.Real where

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.VectorSpace
import DiffGeom.Classes.Fiber
import DiffGeom.Classes.Smooth
import DiffGeom.Classes.Metric
import DiffGeom.Classes.Riem

import Control.Failure

-- |The real line, represented in double precision floating point
newtype DiffReal = DiffReal { unWrap :: Double} deriving (Eq,Show,Read,Ord)

{- |Some manual instances for basic classes.  You can automatically derive these
using the unsafe 'GeneralizedNewtypeDeriving' extension.
-}
instance Num DiffReal where
    (DiffReal x) + (DiffReal y) = DiffReal $ x + y
    (DiffReal x) - (DiffReal y) = DiffReal $ x - y
    (DiffReal x) * (DiffReal y) = DiffReal $ x * y
    negate (DiffReal x) = DiffReal $ negate x
    abs (DiffReal x) = DiffReal $ abs x
    signum (DiffReal x) = DiffReal $ signum x
    fromInteger = DiffReal . fromInteger
instance Fractional DiffReal where
    (DiffReal x) / (DiffReal y) = DiffReal $ x / y
    recip (DiffReal x) = DiffReal $ recip x
    fromRational = DiffReal . fromRational
instance Real DiffReal where
    toRational (DiffReal x) = toRational x
instance Floating DiffReal where
    pi = DiffReal pi
    exp (DiffReal x) = DiffReal $ exp x
    sqrt (DiffReal x) = DiffReal $ sqrt x
    log (DiffReal x) = DiffReal $ log x
    sin (DiffReal x) = DiffReal $ sin x
    tan (DiffReal x) = DiffReal $ tan x
    cos (DiffReal x) = DiffReal $ cos x
    asin (DiffReal x) = DiffReal $ asin x
    atan (DiffReal x) = DiffReal $ atan x
    acos (DiffReal x) = DiffReal $ acos x
    sinh (DiffReal x) = DiffReal $ sinh x
    tanh (DiffReal x) = DiffReal $ tanh x
    cosh (DiffReal x) = DiffReal $ cosh x
    asinh (DiffReal x) = DiffReal $ asinh x
    atanh (DiffReal x) = DiffReal $ atanh x
    acosh (DiffReal x) = DiffReal $ acosh x
    (DiffReal x) ** (DiffReal y) = DiffReal $ x ** y
    logBase (DiffReal x) (DiffReal y) = DiffReal $ logBase x y
instance RealFrac DiffReal where
    properFraction (DiffReal x) = let (a, b) = properFraction x in (a, DiffReal b)

-- |This is the archetypical instance of a tolerance space: one which is nearly an Eq, but where comparisons are "fuzzy"
instance TolSpace DiffReal where
    (DiffReal x) ~~ (DiffReal y) = abs (x - y) <= (abs x + abs y) * 1e-10

instance RealVectorSpace DiffReal where
    (DiffReal x) .+. (DiffReal y) = DiffReal (x + y)
    (DiffReal x) .-. (DiffReal y) = DiffReal (x - y)
    r *. (DiffReal x) = DiffReal (r*x)
    (DiffReal x) ./ r = DiffReal (x/r)
    vzero = DiffReal 0
    vnegate (DiffReal x) = DiffReal $ negate x

instance RealNormedSpace DiffReal where
    norm (DiffReal x) = abs x
    normSquared (DiffReal x) = x * x
instance RealInnerProductSpace DiffReal where
    innerProd (DiffReal x) (DiffReal y) = x * y

-- |Now use `DiffReal` as the Tangent and CoTangentType also
newtype RealBundle = RealBundle (DiffReal, DiffReal) deriving (Show)

-- |Very simple fiber bundle instance
instance FiberBundle RealBundle where
    type BaseType RealBundle = DiffReal
    projBase (RealBundle x) = fst x

-- |Extending arithmetic operations to respect base point
-- TODO: This is probably a general Applicative going on here, and should
-- be put into Fiber.hs
instance VectorBundle RealBundle where
    a *^ (RealBundle (p, DiffReal v)) = RealBundle (p, DiffReal (a*v))
    (RealBundle (p, DiffReal v)) ^* a = RealBundle (p, DiffReal (v*a))
    (RealBundle (p, DiffReal v)) ^+^ (RealBundle (q, DiffReal w))
        | p ~~ q = return $ RealBundle (p, DiffReal (v+w))
        | otherwise = failure VBArithmetic
    vbzero x = RealBundle (x, DiffReal 0)

instance TolSpace RealBundle where
    RealBundle (p, v) ~~ RealBundle (q, w) = p ~~ q && v ~~ w

-- |Self-duality looks just like a product for real numbers
instance DualPairBundles RealBundle RealBundle where
    bundlePairing (RealBundle (p, DiffReal v)) (RealBundle (q, DiffReal w))
        | p ~~ q = return $ v*w
        | otherwise = failure VBDualBasePt

-- |Now ready to define smooth structure on the reals
instance Smooth DiffReal where
    type TangentType DiffReal = RealBundle
    type CoTangentType DiffReal = RealBundle

-- |The usual Riemannian structure on the reals is simply the real product
-- This leads to the usual distance, and the usual Riemannian inner product
instance Metric DiffReal where
    dist (DiffReal x) (DiffReal y) = abs $ x - y

instance Riem DiffReal where
    sharp = id
    flat = id
    expMap (RealBundle (DiffReal p, DiffReal v)) = DiffReal (p + v)
    logMapUnsafe (DiffReal p) (DiffReal q) = RealBundle (DiffReal p, DiffReal (q - p))
    -- |when the logMap is already safe, do this
    logMap p q = return $ logMapUnsafe p q
