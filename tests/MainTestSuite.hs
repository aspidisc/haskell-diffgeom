{-# LANGUAGE FlexibleContexts #-}

module Main (
    main
 ) where

import Test.Tasty

import Data.Monoid

import DiffGeom.Spaces.Vec3Test
import DiffGeom.Spaces.RealTest
import DiffGeom.Spaces.SphereTest
import DiffGeom.Spaces.QuaternionsTest
import DiffGeom.Spaces.RotationsTest

import Test.Generic

main :: IO ()
main = defaultMain tests
  {-where-}
    {-o = mempty {topt_maximum_generated_tests = Just 1000}-}

tests :: TestTree
tests = testGroup "Tests"
  [ testGroup "Spaces"
    [ allVec3Tests
    , allDiffRealTests
    , allSphereTests
    , allQuaternTests
    , allRot3Tests
    ]
  , testGroup "Examples"
    [
    ]
  ]
