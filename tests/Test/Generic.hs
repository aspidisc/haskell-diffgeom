{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}

module Test.Generic where

import Test.Tasty
import Test.Tasty.QuickCheck as QC

import DiffGeom.Classes.Smooth
import DiffGeom.Classes.Fiber

import Data.Tagged

-- |Take a group of tests and perform them together, but keep phantom type
genericTestGroup :: TestName -> [Tagged a TestTree] -> Tagged a TestTree
genericTestGroup x ts = Tagged $ testGroup x $ map unTagged ts

-- |Test a quickcheck property generically using :9
genericTestProperty :: (Arbitrary a, Show a) => TestName -> (a -> Property) -> Tagged a TestTree
genericTestProperty x p = Tagged $ testProperty x p

genericTestPropertyPair :: (Arbitrary a, Show a) => TestName -> ((a,a) -> Property) -> Tagged a TestTree
genericTestPropertyPair x p = Tagged $ testProperty x p

genericTestPropertyTriple :: (Arbitrary a, Show a) => TestName -> ((a,a,a) -> Property) -> Tagged a TestTree
genericTestPropertyTriple x p = Tagged $ testProperty x p

-- |Insert a TangentType into a test
genericTestPropertyTangent :: (Arbitrary (TangentType a), Show (TangentType a)) => TestName -> (TangentType a -> Property) -> Tagged a TestTree
genericTestPropertyTangent x p = Tagged $ testProperty x p
-- |Insert a CoTangentType into a test
genericTestPropertyCoTangent :: (Arbitrary (CoTangentType a), Show (CoTangentType a)) => TestName -> (CoTangentType a -> Property) -> Tagged a TestTree
genericTestPropertyCoTangent x p = Tagged $ testProperty x p
-- |Insert a BaseType from some FiberBundle into a test
genericTestPropertyBaseType :: (Arbitrary (BaseType a), Show (BaseType a)) => TestName -> (BaseType a -> Property) -> Tagged a TestTree
genericTestPropertyBaseType x p = Tagged $ testProperty x p

-- |Also throw in an arbitrary Double
genericTestPropertyDouble :: (Arbitrary a, Show a) => TestName -> ((a, Double) -> Property) -> Tagged a TestTree
genericTestPropertyDouble x p = Tagged $ testProperty x p
-- |Throw in a pair of doubles
genericTestPropertyDoublePair :: (Arbitrary a, Show a) => TestName -> ((a, Double, Double) -> Property) -> Tagged a TestTree
genericTestPropertyDoublePair x p = Tagged $ testProperty x p
-- |Need two a's and a Double
genericTestPropertyPairDouble :: (Arbitrary a, Show a) => TestName -> ((a, a, Double) -> Property) -> Tagged a TestTree
genericTestPropertyPairDouble x p = Tagged $ testProperty x p
