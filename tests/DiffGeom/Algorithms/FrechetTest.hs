module DiffGeom.Algorithms.FrechetTest where

import Test.Framework
import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.Riem
import DiffGeom.Classes.RiemTest

import DiffGeom.Algorithms.Frechet

-- |Enable generic testing
import Test.Generic

-- spaces to test
import DiffGeom.Spaces.Real
import DiffGeom.Spaces.RealTest
import DiffGeom.Spaces.Sphere
import DiffGeom.Spaces.SphereTest

-- Generic tests (apply to any Riem manifold)

-- just make sure a singleton mean doesn't move
propMeanSingleton :: (Riem a) => a -> Property
propMeanSingleton x = property $ fm ~~ x
  where
    fm = snd $ frechetMeanFixed [x] 0.1 10 x 

-- |Summary of generic tests
frechetTests :: (Arbitrary a, Show a, Riem a) => [GenericTest a]
frechetTests =
 [ genericTestProperty     "Mean of single point is itself" propMeanSingleton
 ]

