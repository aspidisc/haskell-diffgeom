module DiffGeom.Classes.SmoothTest where

import Test.Tasty
import Test.QuickCheck

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.TolSpaceTest
import DiffGeom.Classes.Smooth

-- |Enable generic testing
import Test.Generic
import Data.Tagged

-- TODO: Put the fiber bundle and vector bundle tests here for TangentType and
-- CoTangentType

-- |Summary of tests
smoothTests :: (Arbitrary a, Show a, Smooth a) => [Tagged a TestTree]
smoothTests = [ ]

allSmoothTests :: (Arbitrary a, Show a, Smooth a)
    => [Tagged a TestTree]
allSmoothTests =
    genericTestGroup "Smooth" smoothTests : allTolSpaceTests
