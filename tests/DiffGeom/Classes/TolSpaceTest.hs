module DiffGeom.Classes.TolSpaceTest where

import Test.Tasty
import Test.Tasty.QuickCheck as QC
import Test.QuickCheck

import DiffGeom.Classes.TolSpace

-- |Enable generic testing
import Test.Generic
import Data.Tagged

-- |Test the axioms for a tolerance space
propTolReflexiveZero :: (TolSpace a) => a -> Property
propTolReflexiveZero x = property $ x ~~ x

propTolSymmetric :: (TolSpace a) => (a,a) -> Property
propTolSymmetric (x,y) = property $ (x ~~ y) == (y ~~ x)

-- |Summary of tests
tolSpaceTests :: (Arbitrary a, Show a, TolSpace a) => [Tagged a TestTree]
tolSpaceTests =
 [ genericTestProperty "x ~~ x always" propTolReflexiveZero
 , genericTestPropertyPair "~~ is symmetric" propTolSymmetric
 ]

allTolSpaceTests :: (Arbitrary a, Show a, TolSpace a) => [Tagged a TestTree]
allTolSpaceTests = [genericTestGroup "TolSpace" tolSpaceTests]
