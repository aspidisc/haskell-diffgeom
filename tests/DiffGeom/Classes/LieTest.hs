{-# LANGUAGE FlexibleContexts #-}

module DiffGeom.Classes.LieTest where

import Test.Tasty
import Test.QuickCheck

import Data.Monoid

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.TolSpaceTest
import DiffGeom.Classes.SmoothTest
import DiffGeom.Classes.Lie

-- |Enable generic testing
import Test.Generic
import Data.Tagged

-- |'Monoid' tests
propAssoc :: (Monoid a, TolSpace a) => (a, a, a) -> Property
propAssoc (f, g, h) = property $ f <> (g <> h) ~~ (f <> g) <> h
propIdLeft :: (Monoid a, TolSpace a) => a -> Property
propIdLeft g = property $ mempty <> g ~~ g
propIdRight :: (Monoid a, TolSpace a) => a -> Property
propIdRight g = property $ g <> mempty ~~ g

-- |Summary of 'Monoid' tests
monoidTests :: (Arbitrary a, Show a, TolSpace a, Monoid a) => [Tagged a TestTree]
monoidTests =
 [ genericTestPropertyTriple "<> is associative" propAssoc
 , genericTestProperty "mempty is left identity" propIdLeft
 , genericTestProperty "mempty is right identity" propIdRight
 ]

allMonoidTests :: (Arbitrary a, Show a, TolSpace a, Monoid a)
    => [Tagged a TestTree]
allMonoidTests =
    genericTestGroup "Monoid" monoidTests : allTolSpaceTests

-- |Should always be able to log(x,x), and result should be zero
propInvLeft :: (Group a, TolSpace a) => a -> Property
propInvLeft g = property $ invert g <> g ~~ mempty
propInvRight :: (Group a, TolSpace a) => a -> Property
propInvRight g = property $ g <> invert g ~~ mempty

-- |Summary of 'Group' tests
groupTests :: (Arbitrary a, Show a, TolSpace a, Group a) => [Tagged a TestTree]
groupTests =
 [ genericTestProperty "invert gives left inverse" propInvLeft
 , genericTestProperty "invert gives right inverse" propInvRight
 ]

allGroupTests :: (Arbitrary a, Show a, TolSpace a, Group a)
    => [Tagged a TestTree]
allGroupTests =
    genericTestGroup "Group" groupTests : allMonoidTests

-- |'LieGroup' tests
-- TODO: Lie group tests
lieGroupTests :: (Arbitrary a, Show a, TolSpace a, LieGroup a) => [Tagged a TestTree]
lieGroupTests = []

-- |Summary of 'LieGroup' tests
allLieGroupTests :: (Arbitrary a, Show a, TolSpace a, LieGroup a)
    => [Tagged a TestTree]
allLieGroupTests =
    genericTestGroup "LieGroup" lieGroupTests : (allGroupTests ++ allSmoothTests)
