{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}

module DiffGeom.Classes.FiberTest where

import Test.Framework
import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.TolSpaceTest
import DiffGeom.Classes.Fiber

-- |Enable generic testing
import Test.Generic

import Control.Monad (liftM2)
import Data.Maybe (fromMaybe)

-- |Tolerance for floating point comparisons in these tests
tol = 1e-7

-- |floating point compare
fpcmp :: Double -> Double -> Bool
fpcmp x y = abs (x - y) < (abs x + abs y + 1) * tol

cmpMaybe :: TolSpace a => Maybe a -> Maybe a -> Bool
cmpMaybe x y = fromMaybe False (liftM2 (~~) x y)

{- |Test the axioms for vector bundle, similar to vector space axioms
Note that ^+^ is a safe function, so it returns m a, where m is some failure
monad.  This complicates some of the expressions, for instance the additive
associative law.
-}
propPlusCommutes :: (VectorBundle a, TolSpace a) => (a, a) -> Property
propPlusCommutes (x, y) = property $ cmpMaybe (x ^+^ y) (y ^+^ x)
propPlusAssoc :: (VectorBundle a, TolSpace a) => (a, a, a) -> Property
propPlusAssoc (x, y, z) = property $ cmpMaybe la ra
  where
    la = (x ^+^) =<< y ^+^ z
    ra = x ^+^ y >>= (^+^ z)
propZeroLeftId :: (VectorBundle a, TolSpace a) => a -> Property
propZeroLeftId x = property $ maybe False (~~ x) $ vbzero (projBase x) ^+^ x
propZeroRightId :: (VectorBundle a, TolSpace a) => a -> Property
propZeroRightId x = property $ maybe False (~~ x) $ x ^+^ vbzero (projBase x)
propAdditiveInv :: (VectorBundle a, TolSpace a) => a -> Property
propAdditiveInv x = property $ maybe False (~~ vbzero (projBase x)) $ x ^+^ ((-1) *^ x)
propScalarMulCommutes :: (VectorBundle a, TolSpace a) => (a, Double) -> Property
propScalarMulCommutes (x, s) = property $ x ^* s ~~ s *^ x

-- |This test is problematic, as the compiler doesn't know what type to use for
-- vbzero. We could have multiple 'VectorBundle's with the same 'BaseType'
-- Disabling this test for now.
-- propZeroPointFixed :: (VectorBundle a, TolSpace a) => (BaseType a) -> Property
-- propZeroPointFixed x = property $ projBase (vbzero x) ~~ x
-- TODO: The rest of these properties

-- |Summary of tests
vectorBundleTests :: (Arbitrary a, Show a, TolSpace a
    , VectorBundle a
    , Arbitrary (BaseType a)
    , Show (BaseType a)
    ) => [GenericTest a]
vectorBundleTests = 
    [ genericTestPropertyPair "Addition is commutative" propPlusCommutes
    , genericTestPropertyTriple "Addition is associative" propPlusAssoc
    , genericTestProperty "vbzero is additive left identity" propZeroLeftId
    , genericTestProperty "Additive inverse is addition of neg" propAdditiveInv
    , genericTestPropertyDouble "Scalar multiplication commutes" propScalarMulCommutes
    -- , genericTestPropertyBaseType "vbzero fixes point" propZeroPointFixed
    ]

allVectorBundleTests :: (Arbitrary a, Show a, TolSpace a
    , VectorBundle a
    , Arbitrary (BaseType a)
    , Show (BaseType a)
    ) => [GenericTest a]
allVectorBundleTests = 
    genericTestGroup "VectorBundle" vectorBundleTests : allTolSpaceTests
