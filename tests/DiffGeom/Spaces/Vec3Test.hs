module DiffGeom.Spaces.Vec3Test where

import Test.Tasty
import Test.Tasty.QuickCheck as QC

import DiffGeom.Classes.VectorSpace
import DiffGeom.Classes.VectorSpaceTest
import DiffGeom.Spaces.Vec3

import Test.Generic
import Data.Tagged

instance Arbitrary Vec3 where
    arbitrary = do
        x <- arbitrarySizedFractional
        y <- arbitrarySizedFractional
        z <- arbitrarySizedFractional
        return $ Vec3 x y z

vec3Tests :: TestTree
vec3Tests = testGroup "Non-Generic Tests" []

allVec3Tests :: TestTree
allVec3Tests = testGroup "Vec3" $ vec3Tests : [cgp]
  where
    cgp = testGroup "Classes" $ map unTagged (t :: [Tagged Vec3 TestTree])
    t = allRealInnerProductSpaceTests
