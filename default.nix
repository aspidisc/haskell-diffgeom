{ haskellPackages ? (import <nixpkgs> {}).haskellPackages }:
let
  inherit (haskellPackages) cabal cabalInstall_1_18_0_3
    failure
    QuickCheck tasty tastyQuickcheck tagged hlint regexCompat
    ;

in cabal.mkDerivation (self: {
  pname = "diffgeom";
  version = "0.1.0.0";
  src = ./.;
  buildDepends = [
    # As imported above
    failure
  ];
  testDepends = [
        failure QuickCheck tasty tastyQuickcheck tagged hlint regexCompat
  ];
  buildTools = [ cabalInstall_1_18_0_3 ];
  enableSplitObjs = false;
})
